;;; Guix Home Manager.
;;;
;;; Copyright © 2020 Z572 <873216071@qq.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home zsh)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (home)
  #:export (zsh-configuration
            zsh-configuration-env
            zsh-configuration-history
            zsh-configuration-login
            zsh-configuration-logout
            zsh-configuration-profile
            zsh-configuration-rc

            default-zshrc
            default-zshenv
            default-zlogin
            default-zlogout
            default-zprofile

            zsh-home-type))

(define default-zshrc
  `("
if [[ $- != *i* ]]; then
  # Non interative shell. For SSH session, load /etc/profile to get
  # PATH and other variables set up.
    [[ -n \"$SSH_CLIENT\" ]] && source /etc/profile
  return
fi

if [ -n \"$GUIX_ENVIRONMENT\" ]; then
        PS1='%n@%m %~ [env]$ '
else
        PS1='%n@%m %~$ '
fi

fpath=(~/.config/guix/current/share/zsh/site-functions $fpath)
autoload -Uz compinit && compinit -u

alias ls='ls -p --color=auto'
alias ll='ls -l'
alias grep='grep --color=auto'
"))

(define default-zshenv
  `(""))

(define default-zlogin
  `(""))

(define default-zlogout
  `(""))

(define default-zprofile
  `("if [ -f ~/.zshrc ]; then . ~/.zshrc; fi
# Workaround guix bug (not adding load path before processing the command name)
export GUILE_LOAD_PATH=~/.config/guix/current/share/guile/site/3.0:$GUILE_LOAD_PATH
export GUILE_LOAD_COMPILED_PATH=~/.config/guix/current/lib/guile/3.0/site-ccache:\
$GUILE_LOAD_COMPILED_PATH
"))

(define-record-type* <zsh-configuration>
  zsh-configuration make-zsh-configuration
  zsh-configuration?
  (rc      zsh-configuration-rc
           (default default-zshrc))
  (env     zsh-configuration-env
           (default default-zshenv))
  (login   zsh-configuration-login
           (default default-zlogin))
  (logout  zsh-configuration-logout
           (default default-zlogout))
  (profile zsh-configuration-profile
           (default default-zprofile))
  (history zsh-configuration-history))

(define zsh-home-type
  (home-type
   (name 'zsh)
   (extensions
    (list
     (home-extension
      (target root-home-type)
      (compute
       (lambda (config)
         (match config
           (($ <zsh-configuration> rc env login logout profile history)
            (let ((zshenv (apply mixed-text-file "zshenv" env))
                  (zlogin (apply mixed-text-file "zlogin" login))
                  (zlogout (apply mixed-text-file "zlogout" logout))
                  (zshrc (apply mixed-text-file "zshrc" rc))
                  (zprofile (apply mixed-text-file "zprofile" profile)))
              `((".zsh_history" ,history)
                (".zshrc" ,zshrc)
                (".zlogin" ,zlogin)
                (".zlogout" ,zlogout)
                (".zshenv" ,zshenv)
                (".zprofile" ,zprofile))))))))))))
