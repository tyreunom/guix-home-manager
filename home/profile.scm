;;; Guix Home Manager.
;;;
;;; Copyright © 2020 Jelle Licht <jlicht@fsfe.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home profile)
  #:use-module (guix profiles)
  #:use-module (guix gexp)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (home)
  #:export (package-profile-home-type
            source-profile))

(define (packages->profile-entry packages)
  "Return an entry for the profile containing PACKAGES."
  `((".home-profile" ,(profile
		       (content (packages->manifest
				 (delete-duplicates packages eq?)))))))

(define* (source-profile #:optional (profile-path ".home-profile"))
  "Returns a string that can be added to either `.bash_profile` or
`.zprofile` that sources guix profile PROFILE-PATH, making the
contained packages available to the shell. Defaults to sourcing the
guix profile in \"$HOME/.home-profile\"."
  (apply format #f "if [ -f ~s/etc/profile ]; then
	GUIX_PROFILE=~s ; . ~s/etc/profile
	export MANPATH=~s/share/man:$MANPATH
	export INFOPATH=~s/share/info:$INFOPATH
	export XDG_DATA_DIRS=~s/share:$XDG_DATA_DIRS
	export XDG_CONFIG_DIRS=~s/etc/xdg:$XDG_CONFIG_DIRS
fi\n"
         (make-list 7 profile-path)))

(define package-profile-home-type
  (home-type
   (name 'profile)
   (extension-points
    (list (home-extension-point
           (compose concatenate)
           (extend (lambda (v l)
                     (apply append v l))))))
   (extensions
    (list
     (home-extension
      (target root-home-type)
      (compute packages->profile-entry))))
   (default-value '())
   (description "The package-profile home type")))
