;;; Guix Home Manager.
;;;
;;; Copyright © 2020 Z572 <873216071@qq.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (home tmux)
  #:use-module (guix build utils)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (home)
  #:export (tmux-configuration
            tmux-configuration-terminal
            tmux-configuration-prefix
            tmux-configuration-mode-keys
            tmux-configuration-escapeTime
            tmux-configuration-history-limit
            tmux-configuration-mouse?
            tmux-configuration-clock24?
            tmux-configuration-extra-conf
            tmux-home-type))

(define-record-type* <tmux-configuration>
  tmux-configuration make-tmux-configuration
  tmux-configuration?
  (terminal tmux-configuration-terminal
            (default "screen-256color"))
  (prefix tmux-configuration-prefix
          (default #f))
  (mode-keys tmux-configuration-mode-keys
             (default "vi"))
  (escapeTime tmux-configuration-escapeTime
              (default 500))
  (history-limit tmux-configuration-history-limit
                 (default 2000))
  (mouse? tmux-configuration-mouse?
          (default #f))
  (clock24? tmux-configuration-clock24?
            (default #f))
  (extra-conf tmux-configuration-extra-conf
              (default '(""))))

(define tmux-home-type
  (home-type
   (name 'tmux)
   (extensions
    (list
     (home-extension
      (target root-home-type)
      (compute
       (match-lambda
         (($ <tmux-configuration>
             terminal
             prefix
             mode-keys
             escapeTime
             history-limit
             mouse?
             clock24?
             extra-conf)
          (let ((tmux.conf
                 (apply mixed-text-file "tmux.conf"
                        (cons* (format #f "
~{setw -g ~a ~S\n~}
~{set -g ~a ~S\n~}
~a
"
                                       `("clock-mode-style"
                                         ,(if clock24? "24" "12"))
                                       `("escape-time" ,escapeTime
                                         "history-limit" ,history-limit
                                         "default-terminal" ,terminal
                                         "status-keys" ,mode-keys
                                         "mode-keys" ,mode-keys
                                         "mouse" ,(if mouse? "on" "off"))
                                       (if (eq? #f prefix)
                                           ""
                                           (string-append
                                            "unbind C-b\n"
                                            "set -g prefix " prefix)))
                               extra-conf))))
            `((".tmux.conf" ,tmux.conf)))))))))))
