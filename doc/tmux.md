Tmux
====

tmux is a terminal multiplexer. It lets you switch easily between
several programs in one terminal, detach them (they keep running in
the background) and reattach them to a different terminal.

Main Configuration
------------------

Tmux is configured by using the `tmux-home-type` service type.

**Scheme Procedure**: tmux-home-type

The type of service that generates configuration files for tmux.  Its value
is a tmux-configuration object.

**Data Type**: tmux-configuration

Data type that represents the Tmux configuration.  This data type has the
following fields:

* **terminal** (default "screen-256color"): set environment variable
  `TERM`.
* **prefix** (default #f): For keys that control tmux itself. if `#f`
  use default prefix.
* **mode-keys** (default "vi"): VI or Emacs style shortcuts.
* **escapeTime** (default 500): Time in milliseconds for which tmux
  waits after an escape is input.
* **history-limit** (default 2000): Maximum number of lines held in
  window history.
* **mouse?** (default #f): Whether to use mouse.
* **clock24?** (default #f): Whether to use 24 hour clock.
* **extra-conf** (default '("")):  This is a list of strings and
  file-like objects.

Example Configuration
---------------------

```scheme
(user-home
  tmux-home-type
  (tmux-configuration
    (terminal "xterm-256color")
    (prefix "F12")
    (mode-keys "emacs")
    (escapeTime 700)
    (history-limit 5000)
    (mouse? #t)
    (clock24? #t)
    (extra-conf (list "set -g set-titles on\n")))
```
