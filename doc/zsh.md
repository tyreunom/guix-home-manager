Zsh
====

The Z shell (zsh) is a Unix shell that can be used as an interactive
login shell and as a powerful command interpreter for shell scripting.

Main Configuration
------------------

Zsh is configured by using the `zsh-home-type` service type.

**Scheme Procedure**: zsh-home-type

The type of service that generates configuration files for zsh.  Its value
is a zsh-configuration object.

**Data Type**: zsh-configuration

Data type that represents the Zsh configuration.  This data type has the
following fields:

* **rc** (default *default-zshrc*): the content of the `.zshrc` file.  This is
  a list of strings and
* **env** (default *default-zshenv*): the content of the `.zshenv`
  file.
* **login** (default *default-zlogin*): the content of the `.zlogin`
  file.
* **logout** (default *default-zlogout*): the content of the `.zlogout`
  file.
* **profile** (default *default-zprofile*): the content of the `.zprofile`
  file.
* **history** (default #f): the location of the `.zsh_history` file for saving
  history.  The location must be read-write accessible to your user.

Example Configuration
---------------------

```scheme
(user-home
  zsh-home-type
  (zsh-configuration
    (rc (append default-zshrc
                '("alias vim=nvim")))
    (profile (append default-zprofile
                     `("EDITOR=nvim\n"
               ;; Minetest would not work without this :/
                       "export MINETEST_SUBGAME_PATH="
               ,(@@ (gnu packages games) minetest-data)
               "/share/minetest/games/")))
    (history "/data/alice/.zsh_history")))
```
