Using the Home Manager
======================

Installing a new Configuration
------------------------------

You should create a file that contains your home configuration. This file can
easily be kept in version control with git for instance. The file looks
like this:

```scheme
(use-modules (home))

(home "/data/alice" '())
```

Home Reference
--------------

**Scheme Procedure**: (use-home-modules module-name ...)

Import home modules with name _module-name_. For instance,

```scheme
(use-home-modules openbox hexchat)
```

is equivalent to:

```scheme
(use-modules (home openbox)
             (home hexchat))
```

**Data Type**: home

This data type represents the configuration of a home directory.  It is the
type of the data a home configuration file must return.  It is composed of
the following fields:

* **data-directory**: The read-write directory that contains the user files.
* **base-configuration** (default: a base-home-configuration object that takes
    the data-directory value into account, see below): A base-home-configuration
    object that contains configuration for required parts of the home configuration.
* **configurations** (default: '()): A list of additional configurations.

By default, when no base-configuration is given, the configuration contains
instructions for these symlinks to be created:

* `~/.guix-profile` → `${basedir}/.guix-profile`
* `~/.config/guix` → `${basedir}/.config/guix`
* `~/.local` → `${basedir}/.local`
* `~/.cache` → `${basedir}/.cache`

**Data Type**: base-home-configuration

This is the type of a basic home directory configuration that contains information
for basic required configurations.  It contains the following fields:

* **guix-symlink**: The path to the default guix profile, in a read-write directory.
* **guix-config-symlink**: The path to the configuration directory of guix.
* **local-symlink**: The path to a read-write directory that corresponds to the
    .local directory.
* **cache-symlink**: The path to a read-write directory that corresponds to the
    .cache directory.

**Scheme Variable**: root-home-type

The type of the root service, to which every service is folded.  Its value
is an association list that associates paths relative to the home directory
with a path in the filesystem or a file-like object.

It also contains a default extension point, whose value must be an association
list that is concatenated to the already configured association list.
