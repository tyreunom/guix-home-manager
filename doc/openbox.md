Openbox
=======

Openbox is a simple window manager that lets you configure the appearance of
windows, menus, shortcuts and autostarted programs. The `(home openbox)` module
provides configuration options for openbox.

Main Configuration
------------------

Openbox is configured by using the `openbox-home-type` service type.

**Scheme Procedure**: openbox-home-type

The type of service that generates configuration files for openbox.  Its value
is an openbox-configuration object.

**Data Type**: openbox-configuration

Data type that represents the Openbox configuration. This data type has the
following fields:

* **autostart** (default #f): the content of the autostart script. It can be
  #f if no script needs to be run at startup, or a file-like object.
* **environ** (default #f): the content of the environ file. It can be #f if no
  environment variable needs to be loaded at startup, or a file-like object.
* **menus** (default '()): the list of menus that can be added to the root menu.
* **root-elements** (default '()): the list of menu elements that are to be added
  to the root menu.
* **rc** (default #f): the content of the rc file. It can be #f if no specific
  configuration is needed, or a file-like object.

Example Configuration
---------------------

```scheme
(user-home
  openbox-home-type
  (openbox-configuration
    (autostart (local-file "files/autostart"))
    (menus
      (list
        (openbox-menu
          (id "apps-game-menu")
          (label "Games")
          (elements
            (list
              (openbox-element-execute
                (label "MineTest")
                (command (file-append minetest "/bin/minetest")))
              (openbox-element-execute
                (label "harmonist")
                (command (file-append harmonist "/bin/harmonist")))
              (openbox-element-execute
                (label "Xonotic")
                (command (file-append xonotic "/bin/xonotic"))))))))
          (root-elements
            (list
              (openbox-element-menu (id "apps-game-menu"))
              (openbox-element-execute
                (label "Terminal")
                (command (file-append xfce4-terminal "/bin/xfce4-terminal")))
              (openbox-element-execute
                (label "Icecat")
                (command (file-append icecat "/bin/icecat -P --no-remote")))
              (openbox-element-execute
                (label "File Manager")
                (command (file-append pcmanfm "/bin/pcmanfm")))))
          (rc (local-files "files/openbox-rc"))))
```

Menu Configuration
------------------

Menus in Openbox can be configured with the following data types:

**Data Type**: openbox-menu

This data type represents the content of a menu. This data type has the following
fields:

* **id**: a unique identifier used to link to that menu in the parent menu
* **label**: a label shown in the parent menu that opens this menu
* *elements**: the elements of the menu

**Data Type**: openbox-element-menu

This data type represents a menu, as an element of another menu. This data type
has the following fields:

* **id**: the id of the menu that is opened by this element

**Data Type**: openbox-element-execute

This data type represents a menu element that is something to execute when the
user clicks on it. This data type has the following fields:

* **label**: a label shown in the parent menu that executes this element
* **command**: the command to execute when the user clicks on this menu element.
  It can be a string or a g-expression, as seen in the first example.
* **notify?** (default #t): whether to use startupnotify
