Profile
======

This allows users and home-types to install guix packages into guix
package profiles in your immutable home directory.

Main Configuration
------------------

Profile are configured by using the `package-profile-home-type` service type.

**Scheme Variable**: package-profile-home-type

The type of service that generates a guix package profile. Its value
is a list of guix packages.

**Scheme Procedure**: (source-profile [profile-path])

Returns a string that can be added to either `.bash_profile` or
`.zprofile` that sources guix profile _profile-path_, making the
contained packages available to the shell. Defaults to sourcing the
guix profile in `$HOME/.home-profile`.


Example Configuration
---------------------

```scheme
(use-modules (gnu packages emacs))

(user-home
  package-profile-home-type
  (list emacs))
```
