Guix Home Manager Is Deprecated
===============================

Guix home manager was my attempt at creating a home manager for Guix. With
the recent addition of the `guix home` command to guix, this repository is
now deprecated. Note that this repository is not compatible with the new
`guix home`. If you installed it, you should remove it and switch to the
new `guix home`.

To remove this home manager, delete the channel from your `~/.config/guix/channels.scm`
and run `guix pull`. I will add instructions on how to switch to the new
`guix home` as soon as possible.

Guix Home Manager
=================

This is a [Guix](https://www.gnu.org/software/guix/) channel for user configuration
management. Guix itself comes with the possibility of managing system configuration
when installed as a standalone operating system. This project aims at going one
step further and managing your personal configuration entirely with Guix.

For this purpose, the channel extends Guix with more code that will allow you
to generate configuration files in a profile that will replace your home directory.
With Home Manager, your home directory is read-only, so software will not be
able to put any of their garbage in it, unless you specifically allow them to.

This is quite an extreme approach for configuration management, but it follows
the Guix spirit and provides strong guarantee that your configuration is exactly
as you want it to be.

Installing
----------

This is a Guix channel. You will first need to install
[Guix](https://www.gnu.org/software/guix/download/) itself. Then, simply create
a new `~/.config/guix/channels.scm` file with this content:

```scheme
(cons* (channel
        (name 'guix-home-manager)
        (url "https://framagit.org/tyreunom/guix-home-manager.git")
        (introduction
          (make-channel-introduction
            "b5f32950a8fa9c05efb27a0014f9b336bb318d69"
            (openpgp-fingerprint
              "1EFB 0909 1F17 D28C CBF9  B13A 53D4 57B2 D636 EE82"))))
       %default-channels)
```

Then run `guix pull` to pull the new channel.

### Important checks ###

Make sure your guix environment is set up properly. You need to have
`~/.config/guix/current` as the **first** item in your `$PATH` or you're going
to run into troubles. Additionnaly, after running `guix pull`, make sure you
run `hash guix` in any open terminal to make sure bash's cache is cleared of
the old guix binary location.

Usage
-----

This is also [documented](doc/README.md) (and translated) in the documentation
section of this project:

* Il existe aussi une version [francophone](doc/README.fr.md) de la documentation.

### Making some room in your home directory

Your home directory will be completely taken over by Guix.  In particular, when
using the home manager, your home directory is entirely read-only.  A read-only
home directory is not very useful though, so users of the home manager will have
to use a separate directory for their documents, caches and states.  This is
typically `/data/alice` for user alice.

It is not required to set up that directory beforehand, but if you do, you will
not be able to use the home manager until you have completely wiped-out your
home directory (i.e. transfered it to the new directory).  If the directory
does not yet exist, your current home directory is automatically renamed to
that directory, and the home manager starts working.

Basically, you will run (as root):

```bash
mkdir /data
mv /home/alice /data/alice
```

Once that is done, some parts of your home directory will still have to be
read-write. This is mostly `~/.cache`, `~/.local` but also `~/.guix-profile` and
`~/.config/guix`. Inside your new data directory, create them like this, as your
regular user (alice in this example):

```bash
cd /data/alice
mkdir-p .local/share .cache .config
```

Since you have moved your entire home directory, make sure you can still access
your own copy of guix and your user profile by (temporarily) setting your
`$PATH` (make sure it starts with `/data/alice/.config/guix/current/bin`) and
by sourcing the profile with `export GUIX_PROFILE=/data/alice/.guix-profile;
source $GUIX_PROFILE/etc/profile`.  You might also need to run `hash -r`
(no output) for bash to clear all its memorized binary locations.

Finally, create a temporary symbolic link that helps guix find some required
directory (it would otherwise run in a loop trying to create the missing
directories):

```bash
ln -sv /data/alice /var/guix/profiles/per-user/alice/home
# Before running, ensure $HOME does not exist yet.  After this command, it
# should be a symlink.
ln -sv /var/guix/profiles/per-user/alice/home $HOME
```

### Creating the first home generation

To create your first home configuration, you must create a configuration file.
For instance, create `/data/alice/.config/guix/home.scm`:

```scheme
(use-modules (home))

(home
  (data-directory "/data/alice"))
```

This will generate a completely empty home, except for essential configurations,
i. e. writable XDG directories as well as essential guix symlinks.

See the documentation to learn more about the configuration system.

Because of a bug in guix, the `guix home` command will not be found immediately:
the search for the command is done too early, before guix adds the channels
to its search path.  Add them to the guile search path, so guix will be able to
find them.  Note that if you are running bash, the home manager will take
care of that after you install the first generation:

```bash
export GUILE_LOAD_PATH=~/.config/guix/current/share/guile/site/3.0:$GUILE_LOAD_PATH
export GUILE_LOAD_COMPILED_PATH=~/.config/guix/current/lib/guile/3.0/site-ccache:$GUILE_LOAD_COMPILED_PATH
```

To build your first generation of your home environment, run as your regular
user:

```bash
guix home reconfigure /data/alice/.config/guix/home.scm
```

That's it!

Contributing
------------

Contributions are welcome! If there’s a configuration you would like to add, just
fork the repository and create a Merge Request when it is ready.

If you have a history of making quality contributions to GNU Guix and would like
commit access, just ask! Non trivial changes should still go through a simple
Merge Request and code review process, but this channel needs your help to take
over the world!

If you are less into code, we welcome contributions in the form of documentation,
translation, issues, reviews, tips and tricks. Do not hesitate to get in touch if
you have an idea or want to help in any way!
